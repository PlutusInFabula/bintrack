package io.multiversum.bintrack.configs;

import io.multiversum.bintrack.model.Responsabile;
import io.multiversum.bintrack.model.Trasportatore;
import io.multiversum.bintrack.model.Utente;
import io.multiversum.bintrack.repositories.ResponsabileRepo;
import io.multiversum.bintrack.repositories.UtenteRepo;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.logging.Logger;

@Service
public class BootstrapDataPopulator implements InitializingBean {

    @Autowired
    UtenteRepo utenteRepo;

    @Autowired
    ResponsabileRepo responsabileRepo;

    @Override
    @Transactional()
    public void afterPropertiesSet() throws Exception {
        Utente utente = new Utente("Donald", "Duck");
        Utente utente2 = new Utente("Uncle", "Scrooge");
        Responsabile respo = new Trasportatore("Gambadilegno");
        Responsabile respo2 = new Trasportatore("Rockerduck");
        utenteRepo.save(utente);
        utenteRepo.save(utente2);
        responsabileRepo.save(respo);
        responsabileRepo.save(respo2);

    }
}