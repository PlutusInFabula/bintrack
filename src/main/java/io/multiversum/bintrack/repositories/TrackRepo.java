package io.multiversum.bintrack.repositories;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.Responsabile;
import io.multiversum.bintrack.model.Sacchetto;
import io.multiversum.bintrack.model.Traccia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface TrackRepo extends JpaRepository<Traccia, String> {

    ArrayList<Traccia> findAllByResponsabile(Responsabile responsabile);
    ArrayList<Traccia> findAllBySacchetto(Sacchetto sacchetto);
}
