package io.multiversum.bintrack.repositories;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.Utente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtenteRepo extends JpaRepository<Utente, String> {
    Utente findTopByRootHash(String rootHash);

}
