package io.multiversum.bintrack.repositories;

import io.multiversum.bintrack.model.Responsabile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponsabileRepo extends JpaRepository<Responsabile, String> {
    public Responsabile findTopByRootHash(String rootHash);
}
