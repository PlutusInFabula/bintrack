package io.multiversum.bintrack.repositories;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.Sacchetto;
import io.multiversum.bintrack.model.Traccia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface SacchettiRepo extends JpaRepository<Sacchetto, String> {
     ArrayList<Sacchetto> findAllByLottoAndTrackPoints_Empty(Lotto lotto) ;
     ArrayList<Sacchetto> findAllByTrackPointsIn(ArrayList<Traccia> tracce);
     Sacchetto findTopByIdentificativo(String identificativo);
}
