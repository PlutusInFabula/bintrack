package io.multiversum.bintrack.repositories;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.Sacchetto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LottoRepo extends JpaRepository<Lotto, String> {
    Lotto findTopByRootHash(String rootHash);
}
