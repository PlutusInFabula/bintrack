package io.multiversum.bintrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {
        "io.multiversum.mbl",
        "io.multiversum.mbl.model.data",
        "io.multiversum.bintrack"})
@EntityScan(basePackages = {
        "io.multiversum.mbl.model.data",
        "io.multiversum.bintrack"})
@EnableJpaRepositories(basePackages = {
        "io.multiversum.mbl.model.repository",
        "io.multiversum.bintrack"})
public class BintrackApplication {

    public static void main(String[] args) {
        SpringApplication.run(BintrackApplication.class, args);
    }
}
