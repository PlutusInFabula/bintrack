package io.multiversum.bintrack.model;

public enum TipoRifiuto {
    UMIDO, PLASTICA, VETRO, METALLO, CARTA
}
