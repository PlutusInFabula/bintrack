package io.multiversum.bintrack.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.multiversum.bintrack.utilities.RandomID;
import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.util.List;

@Entity
public class Sacchetto extends Chainable{

    @Column
    @DataStateF
    private String identificativo;

    @OneToMany(mappedBy = "sacchetto", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Traccia> trackPoints;

    @ManyToOne
    @DataStateF
    private Indirizzo indirizzo;

    @ManyToOne(fetch = FetchType.LAZY)
    @DataStateF
    @JsonIgnore
    private Lotto lotto;

    public Sacchetto() {
        this.identificativo = RandomID.randomAlphaNumeric(10);
    }

    public Sacchetto(Lotto lotto) {
        this.identificativo = RandomID.randomAlphaNumeric(10);
        this.lotto = lotto;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public List<Traccia> getTrackPoints() {
        return trackPoints;
    }

    public void setTrackPoints(List<Traccia> trackPoints) {
        this.trackPoints = trackPoints;
    }

    public Indirizzo getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(Indirizzo indirizzo) {
        this.indirizzo = indirizzo;
    }

    public Lotto getLotto() {
        return lotto;
    }

    public void setLotto(Lotto lotto) {
        this.lotto = lotto;
    }
}
