package io.multiversum.bintrack.model.wrappers;

import io.multiversum.bintrack.model.TipoTraccia;

public class TracciaWrapper {
    private String rootHashResponsabile;
    private String gps;
    private String identificativo;
    private TipoTraccia tipoTraccia;


    public String getRootHashResponsabile() {
        return rootHashResponsabile;
    }

    public void setRootHashResponsabile(String rootHashResponsabile) {
        this.rootHashResponsabile = rootHashResponsabile;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public TipoTraccia getTipoTraccia() {
        return tipoTraccia;
    }

    public void setTipoTraccia(TipoTraccia tipoTraccia) {
        this.tipoTraccia = tipoTraccia;
    }
}
