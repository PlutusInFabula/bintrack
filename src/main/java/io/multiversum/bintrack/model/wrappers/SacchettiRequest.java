package io.multiversum.bintrack.model.wrappers;

public class SacchettiRequest {
    private String lottoRootHash;
    private String utenteRootHash;
    private int quantitá;

    public String getLottoRootHash() {
        return lottoRootHash;
    }

    public void setLottoRootHash(String lottoRootHash) {
        this.lottoRootHash = lottoRootHash;
    }

    public String getUtenteRootHash() {
        return utenteRootHash;
    }

    public void setUtenteRootHash(String utenteRootHash) {
        this.utenteRootHash = utenteRootHash;
    }

    public int getQuantitá() {
        return quantitá;
    }

    public void setQuantitá(int quantitá) {
        this.quantitá = quantitá;
    }
}

