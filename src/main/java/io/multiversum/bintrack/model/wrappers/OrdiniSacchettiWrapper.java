package io.multiversum.bintrack.model.wrappers;

import io.multiversum.bintrack.model.TipoRifiuto;
import org.springframework.web.bind.annotation.RequestBody;

public class OrdiniSacchettiWrapper {
    private int numeroSacchetti;
    private TipoRifiuto tipoRifiuto;

    public int getNumeroSacchetti() {
        return numeroSacchetti;
    }

    public void setNumeroSacchetti(int numeroSacchetti) {
        this.numeroSacchetti = numeroSacchetti;
    }

    public TipoRifiuto getTipoRifiuto() {
        return tipoRifiuto;
    }

    public void setTipoRifiuto(TipoRifiuto tipoRifiuto) {
        this.tipoRifiuto = tipoRifiuto;
    }
}
