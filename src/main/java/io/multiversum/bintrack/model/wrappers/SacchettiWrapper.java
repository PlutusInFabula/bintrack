package io.multiversum.bintrack.model.wrappers;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.TipoRifiuto;

public class SacchettiWrapper {
    private int numeroSacchetti;
    private TipoRifiuto tipoRifiuto;
    private Lotto lotto;

    public SacchettiWrapper(int numeroSacchetti, TipoRifiuto tipoRifiuto, Lotto lotto) {
        this.numeroSacchetti = numeroSacchetti;
        this.tipoRifiuto = tipoRifiuto;
        this.lotto = lotto;
    }

    public SacchettiWrapper() {
    }

    public int getNumeroSacchetti() {
        return numeroSacchetti;
    }

    public void setNumeroSacchetti(int numeroSacchetti) {
        this.numeroSacchetti = numeroSacchetti;
    }

    public TipoRifiuto getTipoRifiuto() {
        return tipoRifiuto;
    }

    public void setTipoRifiuto(TipoRifiuto tipoRifiuto) {
        this.tipoRifiuto = tipoRifiuto;
    }

    public Lotto getLotto() {
        return lotto;
    }

    public void setLotto(Lotto lotto) {
        this.lotto = lotto;
    }
}
