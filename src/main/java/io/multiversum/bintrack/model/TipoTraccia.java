package io.multiversum.bintrack.model;

public enum TipoTraccia {
    ASSEGNAZIONE, ESPOSIZIONE, PRELIEVO, CONSEGNA, ACCETTAZIONE, INVIO, RIFIUTO
}
