package io.multiversum.bintrack.model;

import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Responsabile extends Chainable{

}
