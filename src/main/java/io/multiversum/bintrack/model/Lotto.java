package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Lotto extends Chainable {

    @Column
    @DataStateF
    private String identificativo;

    @Column
    @DataStateF
    private LocalDateTime dataAssegnazione;

    @Column
    @DataStateF
    private TipoRifiuto tipoLotto;

    @OneToMany(mappedBy = "lotto", fetch = FetchType.EAGER, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    //@DataStateF
    private List<Sacchetto> sacchetti;

    public Lotto() {
    }

    public Lotto(String identificativo, LocalDateTime dataAssegnazione, TipoRifiuto tipoLotto, List<Sacchetto> sacchetti) {
        this.identificativo = identificativo;
        this.dataAssegnazione = dataAssegnazione;
        this.tipoLotto = tipoLotto;
        this.sacchetti = sacchetti;
    }

    public String getIdentificativo() {
        return identificativo;
    }

    public void setIdentificativo(String identificativo) {
        this.identificativo = identificativo;
    }

    public LocalDateTime getDataAssegnazione() {
        return dataAssegnazione;
    }

    public void setDataAssegnazione(LocalDateTime dataAssegnazione) {
        this.dataAssegnazione = dataAssegnazione;
    }

    public TipoRifiuto getTipoLotto() {
        return tipoLotto;
    }

    public void setTipoLotto(TipoRifiuto tipoLotto) {
        this.tipoLotto = tipoLotto;
    }

    public List<Sacchetto> getSacchetti() {
        return sacchetti;
    }

    public void setSacchetti(List<Sacchetto> sacchetti) {
        this.sacchetti = sacchetti;
    }
}
