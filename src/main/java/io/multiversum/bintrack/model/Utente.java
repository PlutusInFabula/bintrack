package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.util.List;

@Entity
public class Utente extends  Responsabile{
    @Column
    @DataStateF
    private String nome;

    @Column
    @DataStateF
    private String cognome;

    @Column
    @DataStateF
    private String cf;

    @OneToMany(mappedBy = "utente", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Indirizzo> indirizzi;

    @OneToMany(mappedBy = "utente", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Pagamento> pagamenti;

    @OneToMany(mappedBy = "responsabile", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Traccia> traccia;

    public Utente(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public List<Indirizzo> getIndirizzi() {
        return indirizzi;
    }

    public void setIndirizzi(List<Indirizzo> indirizzi) {
        this.indirizzi = indirizzi;
    }

    public List<Pagamento> getPagamenti() {
        return pagamenti;
    }

    public void setPagamenti(List<Pagamento> pagamenti) {
        this.pagamenti = pagamenti;
    }

    public List<Traccia> getTraccia() {
        return traccia;
    }

    public void setTraccia(List<Traccia> traccia) {
        this.traccia = traccia;
    }
}
