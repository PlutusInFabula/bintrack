package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.util.List;

@Entity
public class Indirizzo extends Chainable {

    @Column
    @DataStateF
    private String denominazione;

    @Column
    @DataStateF
    private String Tipo;

    @Column
    @DataStateF
    private String numero;

    @Column
    @DataStateF
    private String citta;

    @Column
    @DataStateF
    private String cap;

    @ManyToOne
    @DataStateF
    private Utente utente;

    @OneToMany(mappedBy = "indirizzo", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Sacchetto> sacchetti;

    @ManyToOne
    @DataStateF
    private Zona zona;

    public String getDenominazione() {
        return denominazione;
    }

    public void setDenominazione(String denominazione) {
        this.denominazione = denominazione;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }

    public List<Sacchetto> getSacchetti() {
        return sacchetti;
    }

    public void setSacchetti(List<Sacchetto> sacchetti) {
        this.sacchetti = sacchetti;
    }

    public Zona getZona() {
        return zona;
    }

    public void setZona(Zona zona) {
        this.zona = zona;
    }
}
