package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Traccia extends Chainable {
    @Column
    @DataStateF
    private LocalDateTime timestamp;

    @Column
    @DataStateF
    private String gps;

    @Column
    @DataStateF
    private String nota;

    @ManyToOne
    @DataStateF
    private Responsabile responsabile;

    @ManyToOne
    @DataStateF
    private Sacchetto sacchetto;

    @Column
    @DataStateF
    private TipoTraccia tipoTraccia;

    public Traccia(LocalDateTime timestamp, String gps, String nota, Responsabile responsabile, Sacchetto sacchetto, TipoTraccia tipoTraccia) {
        this.timestamp = timestamp;
        this.gps = gps;
        this.nota = nota;
        this.responsabile = responsabile;
        this.sacchetto = sacchetto;
        this.tipoTraccia = tipoTraccia;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Responsabile getResponsabile() {
        return responsabile;
    }

    public void setResponsabile(Responsabile responsabile) {
        this.responsabile = responsabile;
    }

    public Sacchetto getSacchetto() {
        return sacchetto;
    }

    public void setSacchetto(Sacchetto sacchetto) {
        this.sacchetto = sacchetto;
    }

    public TipoTraccia getTipoTraccia() {
        return tipoTraccia;
    }

    public void setTipoTraccia(TipoTraccia tipoTraccia) {
        this.tipoTraccia = tipoTraccia;
    }
}
