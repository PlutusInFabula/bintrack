package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import java.util.List;

@Entity
public class Zona extends Chainable {

    @Column
    @DataStateF
    private String nome;

    @OneToMany(mappedBy = "zona", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Indirizzo> indirizzi;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Indirizzo> getIndirizzi() {
        return indirizzi;
    }

    public void setIndirizzi(List<Indirizzo> indirizzi) {
        this.indirizzi = indirizzi;
    }
}
