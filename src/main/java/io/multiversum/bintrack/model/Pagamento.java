package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Pagamento extends Chainable {

    @Column
    @DataStateF
    private LocalDateTime data;

    @Column
    @DataStateF
    private String causale;

    @Column
    @DataStateF
    private double importo;

    @ManyToOne
    @DataStateF
    private Utente utente;

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public String getCausale() {
        return causale;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }

    public Utente getUtente() {
        return utente;
    }

    public void setUtente(Utente utente) {
        this.utente = utente;
    }
}
