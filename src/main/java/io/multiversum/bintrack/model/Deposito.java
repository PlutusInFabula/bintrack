package io.multiversum.bintrack.model;

import io.multiversum.mbl.fieldAnnotations.DataStateF;
import io.multiversum.mbl.model.data.Chainable;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.List;

@Entity
public class Deposito  extends Responsabile  {

    @Column
    @DataStateF
    private String name;

    @OneToMany(mappedBy = "responsabile", fetch = FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @DataStateF
    private List<Traccia> traccia;

    public Deposito(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Traccia> getTraccia() {
        return traccia;
    }

    public void setTraccia(List<Traccia> traccia) {
        this.traccia = traccia;
    }
}
