package io.multiversum.bintrack.controllers;

import io.multiversum.bintrack.model.*;
import io.multiversum.bintrack.model.wrappers.SacchettiRequest;
import io.multiversum.bintrack.model.wrappers.SacchettiWrapper;
import io.multiversum.bintrack.repositories.LottoRepo;
import io.multiversum.bintrack.repositories.SacchettiRepo;
import io.multiversum.bintrack.repositories.TrackRepo;
import io.multiversum.bintrack.repositories.UtenteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/Utenti")
public class UtentiManager {

    @Autowired
    SacchettiRepo sacchettiRepo;
    @Autowired
    LottoRepo lottoRepo;

    @Autowired
    UtenteRepo utenteRepo ;
    @Autowired
    TrackRepo trackRepo;

    @Autowired
    ResponsabiliManager responsabiliManager;

    @RequestMapping(path = "/assegna_sacchetti", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SacchettiWrapper>> assegnaSacchetti(@RequestBody SacchettiRequest request){
        Lotto lotto =  lottoRepo.findTopByRootHash(request.getLottoRootHash());
        ArrayList<Sacchetto> sacchettiLiberi = sacchettiRepo.findAllByLottoAndTrackPoints_Empty(lotto);
        Utente utente = utenteRepo.findTopByRootHash(request.getUtenteRootHash());
        int counter = 0;
        for (Sacchetto s : sacchettiLiberi){
            if (counter == request.getQuantitá()){
                break;
            }
            else {
                Traccia traccia = new Traccia(LocalDateTime.now(), "presso Utente", "Consegnato", utente, s, TipoTraccia.ASSEGNAZIONE);
                s.getTrackPoints().add(traccia);
                trackRepo.save(traccia);

            }
        }

        return responsabiliManager.trovaSacchettiDaResponsabile(utente.getRootHash());

    }

    @RequestMapping(path = "/sacchetti_disponibili", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <List<SacchettiWrapper>> sacchettiDisponibiliAdUtente(@RequestBody String rootHashUtente){

        return responsabiliManager.trovaSacchettiDaResponsabile(rootHashUtente);
    }

    @RequestMapping(path = "/sacchetti_usati", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <List<SacchettiWrapper>> sacchettiUsatiDaUtente(@RequestBody String rootHashUtente, @RequestBody int anno){

        ArrayList<SacchettiWrapper> response = new ArrayList<>();
        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/findAllUtenti", method = RequestMethod.GET)
    public ResponseEntity <List<Utente>> findAll (){

        return ResponseEntity.ok(utenteRepo.findAll());
    }

}
