package io.multiversum.bintrack.controllers;

import io.multiversum.bintrack.model.*;
import io.multiversum.bintrack.model.wrappers.SacchettiWrapper;
import io.multiversum.bintrack.repositories.ResponsabileRepo;
import io.multiversum.bintrack.repositories.SacchettiRepo;
import io.multiversum.bintrack.repositories.TrackRepo;
import org.hibernate.validator.internal.util.CollectionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/responsabili")
public class ResponsabiliManager {

    @Autowired
    ResponsabileRepo responsabileRepo;
    @Autowired
    SacchettiRepo sacchettiRepo;
    @Autowired
    TrackRepo trackRepo;

    @RequestMapping(path = "/trova_sacchetti", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SacchettiWrapper>> trovaSacchettiDaResponsabile(@RequestBody String rootHashResponsabile){

        Responsabile responsabile = responsabileRepo.findTopByRootHash(rootHashResponsabile);
        ArrayList<Traccia> tracce = trackRepo.findAllByResponsabile(responsabile);
        ArrayList<Sacchetto> sacchettiDaResponsabile = sacchettiRepo.findAllByTrackPointsIn(tracce);

        ArrayList<SacchettiWrapper> response = new ArrayList<>();

        for (Sacchetto s : sacchettiDaResponsabile){
            boolean existsWrapper = false;
            for (SacchettiWrapper sw : response){

                if (sw.getLotto() == s.getLotto() && sw.getTipoRifiuto() == s.getLotto().getTipoLotto()){
                    sw.setNumeroSacchetti(sw.getNumeroSacchetti()+1);
                    existsWrapper = true;
                break;
                }

            }  if (!existsWrapper ){
                SacchettiWrapper nsw = new SacchettiWrapper();
                nsw.setTipoRifiuto(s.getLotto().getTipoLotto()) ;
                nsw.setNumeroSacchetti(1);
                nsw.setLotto( s.getLotto());
                response.add(nsw);
            }

        }

        return ResponseEntity.ok( response);
    }

    @RequestMapping(path = "/findAllResponsabili", method = RequestMethod.GET)
    public ResponseEntity <List<Responsabile>> findAll (){

        return ResponseEntity.ok(responsabileRepo.findAll());
    }


}
