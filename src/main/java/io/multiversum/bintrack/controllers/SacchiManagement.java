package io.multiversum.bintrack.controllers;

import io.multiversum.bintrack.model.Lotto;
import io.multiversum.bintrack.model.Sacchetto;
import io.multiversum.bintrack.model.wrappers.OrdiniSacchettiWrapper;
import io.multiversum.bintrack.model.wrappers.SacchettiWrapper;
import io.multiversum.bintrack.repositories.LottoRepo;
import io.multiversum.bintrack.repositories.SacchettiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/sacchi")
public class SacchiManagement {

    @Autowired
    SacchettiRepo sacchettiRepo;

    @Autowired
    LottoRepo lottoRepo;

    @RequestMapping(path = "/sacchetti_disponibili", method = RequestMethod.GET)
    public ResponseEntity <List<SacchettiWrapper>> sacchettiDisponibili(){
        ArrayList<SacchettiWrapper> response = new ArrayList<>();

        List<Lotto> lotti = lottoRepo.findAll();

        for (Lotto l : lotti){
            ArrayList<Sacchetto> actualSacchetti = sacchettiRepo.findAllByLottoAndTrackPoints_Empty(l);
            int numeroSacchettiLiberi = actualSacchetti.size();
            if (numeroSacchettiLiberi>0){
                SacchettiWrapper wrapper = new SacchettiWrapper(numeroSacchettiLiberi, l.getTipoLotto(), l);
                response.add(wrapper);
            }
        }


        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/ordina_sacchetti", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SacchettiWrapper> ordinaSacchetti(@RequestBody OrdiniSacchettiWrapper ordine){


        ArrayList<Sacchetto> sacchetti = new ArrayList<>();
        Lotto lotto_1 = new Lotto("Lotto numero " + Math.random()* 9999 + 1,  LocalDateTime.now(), ordine.getTipoRifiuto(), sacchetti);

        for (int i = 0; i< ordine.getNumeroSacchetti(); i++){
                        Sacchetto sacchetto = new Sacchetto(lotto_1);
                        sacchetti.add(sacchetto);
        }

        lotto_1 = lottoRepo.save(lotto_1);


        SacchettiWrapper response = new SacchettiWrapper(ordine.getNumeroSacchetti(), ordine.getTipoRifiuto(), lotto_1);

        return ResponseEntity.ok(response);
    };





}
