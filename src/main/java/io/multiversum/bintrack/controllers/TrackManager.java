package io.multiversum.bintrack.controllers;


import io.multiversum.bintrack.model.*;
import io.multiversum.bintrack.model.wrappers.TracciaWrapper;
import io.multiversum.bintrack.repositories.ResponsabileRepo;
import io.multiversum.bintrack.repositories.SacchettiRepo;
import io.multiversum.bintrack.repositories.TrackRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/tracce")
public class TrackManager {

    @Autowired
    ResponsabileRepo responsabileRepo;
    @Autowired
    SacchettiRepo sacchettiRepo;
    @Autowired
    TrackRepo trackRepo;

    @RequestMapping(path = "/aggiungi_traccia", method = RequestMethod.POST, consumes= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Traccia>> inserisciNuovaTraccia(@RequestBody TracciaWrapper traccia){

        Responsabile responsabile = responsabileRepo.findTopByRootHash(traccia.getRootHashResponsabile());
        Sacchetto sacchetto = sacchettiRepo.findTopByIdentificativo(traccia. getIdentificativo());
        Traccia nuovaTraccia = new Traccia(LocalDateTime.now(), traccia.getGps(), null, responsabile, sacchetto, traccia.getTipoTraccia());

        trackRepo.save(nuovaTraccia);

        ArrayList<Traccia> response = trackRepo.findAllBySacchetto(sacchetto);

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/trecce_sacchetto", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Traccia>> tracciaSacchetto (@RequestBody String identificativo){
        Sacchetto sacchetto = sacchettiRepo.findTopByIdentificativo(identificativo);
        ArrayList<Traccia> response = trackRepo.findAllBySacchetto(sacchetto);
        return ResponseEntity.ok(response);
    }

}
